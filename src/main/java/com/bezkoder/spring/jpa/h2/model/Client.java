package com.bezkoder.spring.jpa.h2.model;

import javax.persistence.*;

@Entity
@Table(name = "client")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "idCode")
	private String idCode;

	@Column(name = "idType")
	private String idType;

	@Column(name = "firstLastname")
	private String firstLastname;

	@Column(name = "secondLastname")
	private String secondLastname;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "otherName")
	private String otherName;

	@Column(name = "email")
	private String email;

	@Column(name = "country")
	private String country;

	@Column(name = "area")
	private String area;

	@Column(name = "regDate")
	private String regDate;

	@Column(name = "editDate")
	private String editDate;

	@Column(name = "status")
	private boolean status;

	public Client() {

	}

	public Client(boolean status) {
		this.status = status;
	}

	public Client(	String idCode,
					String idType,
					String firstLastname,
					String secondLastname,
					String firstName,
					String otherName,
					String email,
					String country,
					String area,
					String regDate,
					String editDate,
					boolean status
					) {
		this.idCode = idCode;
		this.idType = idType;
		this.firstLastname = firstLastname;
		this.secondLastname = secondLastname;
		this.firstName = firstName;
		this.otherName = otherName;
		this.email = email;
		this.country = country;
		this.area = area;
		this.regDate = regDate;
		this.editDate = editDate;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getFirstLastname() {
		return firstLastname;
	}
	public void setFirstLastname(String firstLastname) {
		this.firstLastname = firstLastname;
	}

	public String getSecondLastname() {
		return secondLastname;
	}
	public void setSecondLastname(String secondLastname) {
		this.secondLastname = secondLastname;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getOtherName() {
		return otherName;
	}
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}

	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getEditDate() {
		return editDate;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}

	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return 	"Client [idCode=" + idCode +
				", firstLastname=" + firstLastname +
				", secondLastname=" + secondLastname +
				", firstName=" + firstName +
				", otherName=" + otherName +
				", email=" + email +
				", country=" + country +
				", area=" + area +
				", regDate=" + regDate +
				", editDate=" + editDate +
				", status=" + status + "]"
				;
	}

}
