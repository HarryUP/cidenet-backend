package com.bezkoder.spring.jpa.h2.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bezkoder.spring.jpa.h2.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
  List<Client> findByFirstNameContaining(String firstName);
  List<Client> findByEmail(String email);
}
