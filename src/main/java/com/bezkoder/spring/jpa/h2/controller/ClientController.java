package com.bezkoder.spring.jpa.h2.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.jpa.h2.model.Client;
import com.bezkoder.spring.jpa.h2.repository.ClientRepository;

@CrossOrigin(origins = "http://freeaspect.com:8081")
@RestController
@RequestMapping("/api")
public class ClientController {
    final String REGEX_CAPSWITHSPACE = "[A-Z\\s]+";
    final String REGEX_CAPSNOSPACE = "[A-Z]+";
    final String REGEX_ID = "[A-Za-z0-9\\-]+";
    final boolean STATUS = true; //always true, not edit

	@Autowired
	ClientRepository clientRepository;

	@GetMapping("/clients")
	public ResponseEntity<List<Client>> getAllClients(@RequestParam(required = false) String firstName) {
		try {
			List<Client> clients = new ArrayList<Client>();

			if (firstName == null)
				clientRepository.findAll().forEach(clients::add);
			else
				clientRepository.findByFirstNameContaining(firstName).forEach(clients::add);

			if (clients.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(clients, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/clients/{id}")
	public ResponseEntity<Client> getClientById(@PathVariable("id") long id) {
		Optional<Client> clientData = clientRepository.findById(id);

		if (clientData.isPresent()) {
			return new ResponseEntity<>(clientData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    public String checkName(String data, String regex, int max_str, boolean to_upper){

			String s = data.strip();
			
			if(to_upper)
				s = s.toUpperCase();

			System.out.println(s);
			if( (s.length() <= max_str) &&	//MAX letras
				Pattern.matches(regex,s)	//Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.
											//espacio para apellidos compuestos
			){
				return s;
			}else{
				return null;
			}

    }

	public String generateEmail(Long id, String firstName, String firstLastname, String country){
		//find emails similar and in they have a seq
        String emailName = (firstName+"."+firstLastname).toLowerCase().replaceAll("\\s+", ""); //eliminar espacios
        String dominio = "@cidenet.com."+country;
		String generatedEmail = "";

		List<Client> clientData = clientRepository.findByEmail(emailName+dominio);

        if(emailName.length() <= 300){
            if(clientData.size() > 0){
                generatedEmail = emailName+"."+id+dominio;
            } else {
                generatedEmail = emailName+dominio;
            }
        }

		return generatedEmail;
	}

	public Client generateClient(Client client, boolean isNew){
		String idCode = checkName(client.getIdCode(),REGEX_ID,20,false);
		String idType = checkName(client.getIdType(),REGEX_CAPSNOSPACE,2,true);
		String firstLastname = checkName(client.getFirstLastname(),REGEX_CAPSWITHSPACE,20,true);
		String secondLastname = checkName(client.getSecondLastname(),REGEX_CAPSWITHSPACE,20,true);
		String firstName = checkName(client.getFirstName(),REGEX_CAPSNOSPACE,20,true);
		String otherName = checkName(client.getOtherName(),REGEX_CAPSWITHSPACE,50,true);
		String country = checkName(client.getCountry(),REGEX_CAPSNOSPACE,2,true);
		String area = checkName(client.getArea(),REGEX_CAPSNOSPACE,3,true);
		
		boolean status = true;

		if(isNew){
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
			LocalDateTime dateNow = LocalDateTime.parse(client.getRegDate(), formatter);

			LocalDate minDate = LocalDate.now().minusMonths(1).minusDays(0);

			Period period = Period.between(dateNow.toLocalDate(), minDate);
			int years = Math.abs(period.getYears());
			int months = Math.abs(period.getMonths());
			int days = Math.abs(period.getDays());

		 	if(years < 1 && ((months == 1 && days == 0) || months == 0) ){
		 		client.setEditDate(dateNow.format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss")));
		 	} else {
				status = false;
			}
		}

		return new Client(
			idCode,
			idType,
			firstLastname,
			secondLastname,
			firstName,
			otherName,
			isNew ? null : client.getEmail(),
			country,
			area,
			client.getRegDate(),
			client.getEditDate(),
			status
		);
	}

	@PostMapping("/clients")
	public ResponseEntity<Client> createClient(@RequestBody Client client) {
		try {
			client = generateClient(client,true);

			if (client.getIdCode() == null || client.getFirstLastname() == null || client.getSecondLastname() == null || client.getFirstName() == null){
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);			
			} else {
				if(client.getStatus()){
					Client resClient = clientRepository.save(client);

					resClient.setEmail(
						generateEmail(
							client.getId(),
							client.getFirstName(),
								client.getFirstLastname(),
							client.getCountry().toLowerCase()
						)
					);

					return new ResponseEntity<>(clientRepository.save(client), HttpStatus.CREATED);
				} else {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			}

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@PutMapping("/clients/{id}")
	public ResponseEntity<Client> updateClient(@PathVariable("id") long id, @RequestBody Client client) {
		Optional<Client> clientData = clientRepository.findById(id);

		if (clientData.isPresent()) {
			Client _client = clientData.get();

			client = generateClient(client,false);

			_client.setIdCode(client.getIdCode() == null ? _client.getIdCode() : client.getIdCode());
			_client.setIdType(client.getIdType() == null ? _client.getIdType() : client.getIdType());
			_client.setFirstLastname(client.getFirstLastname() == null ? _client.getFirstLastname() : client.getFirstLastname());
			_client.setSecondLastname(client.getSecondLastname() == null ? _client.getSecondLastname() : client.getSecondLastname());
			_client.setFirstName(client.getFirstName() == null ? _client.getFirstName() : client.getFirstName());
			_client.setOtherName(client.getOtherName() == null ? _client.getOtherName() : client.getOtherName());
			_client.setEmail(
						
						generateEmail(
							_client.getId(),
							client.getFirstName(),
							client.getFirstLastname(),
							client.getCountry().toLowerCase()
						
					)
			);
			_client.setCountry(client.getCountry() == null ? _client.getCountry() : client.getCountry());
			_client.setArea(client.getArea() == null ? _client.getArea() : client.getArea());
			_client.setRegDate(client.getRegDate() == null ? _client.getRegDate() : client.getRegDate());
			_client.setEditDate(client.getEditDate() == null ? LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss")) : client.getEditDate());

			return new ResponseEntity<>(clientRepository.save(_client), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/clients/{id}")
	public ResponseEntity<HttpStatus> deleteClient(@PathVariable("id") long id) {
		try {
			clientRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/clients")
	public ResponseEntity<HttpStatus> deleteAllClients() {
		try {
			clientRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
